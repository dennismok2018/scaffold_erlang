# Description

Simple script written with escript to scaffold OTP callback modules.

# Usage

```
scaffold_erlang server|statem|event Module_Name
```

# Example

```
scaffold_erlang server hello_world > hello_world.erl
```

